use std::{thread, time::Duration};

use sysinfo::{System, SystemExt};

fn main() {
    let mut sys = System::new();

    // Number of CPUs:
    loop {
        sys.refresh_memory();

        let memory_percentage: f64 =
            100 as f64 * (sys.used_memory() as f64 / sys.total_memory() as f64);

        println!("memperc|string|{:.0}", memory_percentage);
        if sys.total_swap() > 0 {
            let swap_percentage: f64 = 100 as f64 * (sys.used_swap() as f64 / sys.total_swap() as f64);
            println!("swapperc|string|{:.0}", swap_percentage);
            println!("swapstate|bool|true");
        } else {
            println!("swapstate|bool|false");
        }
        println!("");

        // Sleeping to give the system time to run for long
        // enough to have useful information.
        thread::sleep(Duration::from_secs(3));
    }
}
